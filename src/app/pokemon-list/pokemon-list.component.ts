import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  pokemons: any[] = [];
  selectedpokemons: any[] = [];
  pokemonName: string = '';
  page = 1;
  totalPokemons: number = 0

  constructor(
    private pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    this.getPokemons();
  }
  getPokemons() {
    this.pokemonService.fetchPokemons(12, this.page + 0)
    .subscribe((response: any) => {
      this.totalPokemons = response.count;

      response.results.forEach((result: { name: string; }) => {
        this.pokemonService.fetchMorePokemons(result.name)
        .subscribe((uniqueResponse: any) => {
          this.pokemons.push(uniqueResponse);
          console.log(this.pokemons)
        });
      })
    });
  }

  public addPokemonToCollection(pokemon: any): void {

    //array is emptied when we move backa dn forth  
    this.selectedpokemons.push(pokemon);
    localStorage.setItem('pokemon', JSON.stringify(this.selectedpokemons));
    window.alert(`${pokemon.name} is added to the collection`)

 

    //overwrites beacuse same key
    //localStorage.setItem(pokemon.name, JSON.stringify(pokemon));



  }

}
