import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-selected-pokemon-list',
  templateUrl: './selected-pokemon-list.component.html',
  styleUrls: ['./selected-pokemon-list.component.css']
})
export class SelectedPokemonListComponent implements OnInit {

  pokemons: any[] = [];


  constructor() { }

  ngOnInit(): void {
    this.getPokemons();
    console.log(this.pokemons);
  }

  getPokemons() {
   
    //if prevLocalstorage length was diff then current local storage  length 
   
  this.pokemons = JSON.parse(localStorage.getItem('pokemon') || '[]');
  


  }

  public checkLocalStorage() {
    if (localStorage.getItem('pokemon') != null) {
      return true
    } else {
      return false
    }
  }

 

}
