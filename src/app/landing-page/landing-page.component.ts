import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {

  value: string | undefined


  public username: string = ''

  constructor( public router: Router) {

   }

  ngOnInit(): void {
    if (localStorage.getItem('username') != null) {
      this.router.navigate(['pokemons'])
    }
  }

  public onUserButtonClicked() : void {
      if(this.value !== undefined) {
        this.username = this.value;
        localStorage.setItem('username', this.username);

      }

      if (localStorage.getItem('username') != null) {
        this.router.navigate(['pokemons'])
      }

 

  }

  public checkLocalStorage() {
    if (localStorage.getItem('username') != null) {
      return true
    } else {
      return false
    }
    
  }




}
