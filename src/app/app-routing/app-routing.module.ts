import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PokemonListComponent } from '../pokemon-list/pokemon-list.component';
import { LandingPageComponent } from '../landing-page/landing-page.component';
import { SelectedPokemonListComponent } from '../selected-pokemon-list/selected-pokemon-list.component';
import { AuthGuardService } from '../services/auth-guard.service';


let LoggedInGuard : boolean = false;

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing'
  },
  {
    path: 'landing',
    component: LandingPageComponent
  },
  {
    path: 'pokemons',
    component: PokemonListComponent, canActivate:[AuthGuardService]

  },
  {
    path: 'pokemons/selected',
    component: SelectedPokemonListComponent, canActivate:[AuthGuardService]
  }

]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
